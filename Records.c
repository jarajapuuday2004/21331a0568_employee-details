#include<stdio.h>
int main()
{
    FILE *fptr;
    int i, n, empno,age;
    float salary;
    char name[20],id[20];
    fptr = fopen("EMPLOYEE.TXT", "w");
    printf("Enter the number of employees : ");
    scanf("%d", &n);
    for(i = 0; i < n; i++)
    {
        printf("\nEnter the employee number : ");
        scanf("%d", &empno);
        printf("\nEnter the name of the employee : ");
        scanf("%s", name);
        printf("Enter the employee age : ");
        scanf("%d", &age);
        printf("Enter the employee id : ");
        scanf("%s", id);
        printf("Enter the employee salary : ");
        scanf("%f", &salary);
        fprintf(fptr, "%d %s %d %s %f \n", empno,name,age,id,salary);
    }
	fclose(fptr);
	fptr = fopen("EMPLOYEE.TXT", "r");
	printf("\nEmpNo.\t\t      Name\t\t      Age\t\t       Id\t\t       Salary\n\n");
	for(i = 0; i < n; i++)
    {
    	fscanf(fptr,"%d%s%d%s%f\n", &empno,name,&age,id,&salary);
    	printf("%d \t\t       %s \t\t     %d \t\t    %s \t\t     %.2f \n", empno, name, age, id, salary);
    }
    fclose(fptr);
}